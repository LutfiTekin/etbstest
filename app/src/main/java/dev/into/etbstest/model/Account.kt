package dev.into.etbstest.model


import com.google.gson.annotations.Expose
import dev.into.etbstest.util.ACCOUNT_BASE_URL
import dev.into.etbstest.util.AccountAuthService
import dev.into.etbstest.util.defaultOkHttpClient
import dev.into.etbstest.util.logWith
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception

const val TYPE_PERSON = "0"
const val TYPE_FIRM = "1"

class Account(
    val accountName: String,
    val accountSurname: String,
    val accountTitle: String,
    val accountIdentityNumber: String,
    val accountEmail: String,
    val accountPhone: String,
    val accountPassword: String,
    val accountType: String
) {

    val toMap: Map<String,String>
        get() {
            return mapOf("accountName" to accountName,
                "accountSurname" to accountSurname,
                "accountTitle" to accountTitle,
                "accountIdentityNumber" to accountIdentityNumber,
                "accountTaxNumber" to accountTaxNumber,
                "accountEmail" to accountTitle,
                "accountPhone" to accountPhone,
                "accountPassword" to accountPassword,
                "accountType" to accountType)
        }

    val accountTaxNumber: String
        get() {
            return accountIdentityNumber
        }

    val loginCredentials: Login
        get() {
            return Login(this.accountEmail,this.accountPassword)
        }



    fun signUp(finishedWithoutError: (Boolean,String?) -> Unit){
        val retrofit = Retrofit.Builder()
            .baseUrl(ACCOUNT_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(AccountAuthService::class.java)
        val serviceCall = service.createUser(this)

        try {
            serviceCall.enqueue(object : Callback<Result<String>>{
                override fun onResponse(call: Call<Result<String>>, response: Response<Result<String>>) {
                    "Result is ${response.body()}" logWith "ETBS Account.kt"
                    val result = response.body() ?: run {
                        finishedWithoutError(false,null)
                        return
                    }
                    if (result.code == SUCCESS_CODE){
                        finishedWithoutError(true,null)
                    }else{
                        finishedWithoutError(false,result.message)
                    }
                }

                override fun onFailure(call: Call<Result<String>>, t: Throwable) {
                    finishedWithoutError(false,null)
                    "Failed: ${call.request()} ${t.message}" logWith "Account.Kt"
                }
            })
        }catch (e: Exception){
            e.printStackTrace()
        }
    }


    class Login(private val accountEmail: String,
                      private val accountPassword: String){

        fun logIn(loggedInWithoutError: (Boolean) -> Unit){
            val retrofit = Retrofit.Builder()
                .baseUrl(ACCOUNT_BASE_URL)
                .client(defaultOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            val service = retrofit.create(AccountAuthService::class.java)
            val serviceCall = service.loginUser(this.accountEmail)
            serviceCall.enqueue(object : Callback<Result<String>>{
                override fun onResponse(
                    call: Call<Result<String>>,
                    response: Response<Result<String>>
                ) {
                    "Response: ${response.body()}" logWith "Account.Kt"
                    val result = response.body() ?: run {
                        loggedInWithoutError(false)
                        return
                    }
                    loggedInWithoutError(accountPassword == result.data)
                }

                override fun onFailure(call: Call<Result<String>>, t: Throwable) {
                    "Failed: ${call.request()} ${t.message}" logWith "Account.Kt"
                }
            }

            )
        }
    }


}
