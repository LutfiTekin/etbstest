package dev.into.etbstest.model

data class TokenRequest(val grant_type: String, val username: String, val password: String)