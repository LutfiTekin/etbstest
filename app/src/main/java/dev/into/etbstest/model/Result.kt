package dev.into.etbstest.model

import android.R.attr.data


const val TECHNICAL_ERROR = "TE-101"
const val BUSINESS_ERROR = "BE-101"
const val SUCCESS_CODE = "100"

class Result<T>(t: T?) {

    var data = t
    lateinit var message: String
    var code: String = SUCCESS_CODE

}