package dev.into.etbstest.model

import androidx.annotation.DrawableRes
import androidx.annotation.IntegerRes
import androidx.annotation.StringRes
import dev.into.etbstest.R

const val ACTION_GOODS_ACCEPTANCE = 0
const val ACTION_SALES = 1
const val ACTION_REFUND = 2
const val ACTION_SALE_CANCEL = 3
const val ACTION_REMOVE = 4


data class NavAction(val id: Int,
                     @StringRes val title: Int,
                     @DrawableRes val icon: Int){

    interface SelectedListener{
        fun onSelected(action: Int)
    }

    companion object{
        val list = listOf(
            NavAction(ACTION_GOODS_ACCEPTANCE, R.string.nav_action_goods_accp, R.mipmap.acc_goods),
            NavAction(ACTION_SALES, R.string.nav_action_sales, R.mipmap.sell),
            NavAction(ACTION_REFUND, R.string.nav_action_refund, R.mipmap.refund),
            NavAction(ACTION_SALE_CANCEL, R.string.nav_action_sale_cancel, R.drawable.ic_close_black_24dp),
            NavAction(ACTION_REMOVE, R.string.nav_action_trash, R.drawable.ic_delete_forever_black_24dp))
    }
}