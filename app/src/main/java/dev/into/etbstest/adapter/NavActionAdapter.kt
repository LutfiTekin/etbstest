package dev.into.etbstest.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dev.into.etbstest.R
import dev.into.etbstest.model.NavAction
import dev.into.etbstest.viewholder.NavActionViewHolder

class NavActionAdapter (private val items: List<NavAction>,
                        private var listener: NavAction.SelectedListener? = null): RecyclerView.Adapter<NavActionViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NavActionViewHolder {
        return NavActionViewHolder(LayoutInflater.from(parent.context),parent, R.layout.nav_card, listener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: NavActionViewHolder, position: Int) {
        holder.bind(items[position])
    }


}