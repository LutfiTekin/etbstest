package dev.into.etbstest.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import dev.into.etbstest.*
import dev.into.etbstest.util.beginDelayedTransition
import dev.into.etbstest.util.isValidEmail
import dev.into.etbstest.util.testRecoverPassword
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.activity_recover_password.*
import kotlinx.android.synthetic.main.activity_recover_password.loginFlowBackground
import kotlin.random.Random

class RecoverPasswordActivity : Base() {

    var passwordSent = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recover_password)
        setupUI()
    }

    /**
     * Setup UI elements before activity loads the ui
     */
    private fun setupUI() {
        //Load background image via glide with blur effect
        Glide.with(context)
            .load(R.mipmap.login_bg)
            .transform(BlurTransformation(40), CenterCrop())
            .into(loginFlowBackground)
        backButton.setOnClickListener { this.onBackPressed() }
        buttonSend.setOnClickListener {
            if (passwordSent){
                startActivity(Intent(this, LandingActivity::class.java))
            } else {
                recoverPassword()
            }
        }
    }

    private fun recoverPassword(){
        if (inputEmail.text.toString().isValidEmail.not()){
            inputEmail.error = getString(R.string.til_error_invalid_email)
            return
        }
        Toast.makeText(context,getString(R.string.toast_wait),Toast.LENGTH_LONG).show()
        testRecoverPassword { success ->
            //Setup parent for animation
            (recoverPasswordTitle.parent as ViewGroup).beginDelayedTransition()
            if (success) { //Password reset is successful
                passwordSent = true
                recoverPasswordTitle.text = getString(R.string.password_reset_title)
                recoverPasswordDescription.text = getString(R.string.password_reset_description)
                inputEmail.visibility = View.GONE
                buttonSend.text = getString(R.string.ok)
                buttonSend.backgroundTintList = ContextCompat
                    .getColorStateList(this@RecoverPasswordActivity, R.color.colorAccent)
            } else { //Password reset failed
                recoverPasswordTitle.text = getString(R.string.password_reset_error_title)
                recoverPasswordDescription.text = String.format(
                    getString(R.string.password_reset_error_description),
                    Random.nextInt(200)
                )
                buttonSend.text = getString(R.string.try_again)
                buttonSend.backgroundTintList = ContextCompat
                    .getColorStateList(this@RecoverPasswordActivity, R.color.colorError)
            }
        }
    }




}
