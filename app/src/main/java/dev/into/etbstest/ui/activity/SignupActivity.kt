package dev.into.etbstest.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.google.android.material.tabs.TabLayout
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import dev.into.etbstest.R
import dev.into.etbstest.model.Account
import dev.into.etbstest.model.TYPE_FIRM
import dev.into.etbstest.model.TYPE_PERSON
import dev.into.etbstest.util.*
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.activity_signup.*

const val TAB_FIRM = 0
const val TAB_PERSON = 1

class SignupActivity : Base() {

    private var selectedTab = TAB_FIRM

    private val formInputFields by lazy {
        listOf(
            inputName,
            inputUserid,
            inputEmail,
            inputMobile,
            inputTitle,
            inputPassword,
            inputPasswordRepeat
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        setupUI()
    }

    /**
     * Setup UI elements before activity loads the ui
     */
    private fun setupUI() {
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.navigationIcon?.setColorFilterCompat(android.R.color.white)
        //Load background image via glide with blur effect
        Glide.with(context)
            .load(R.mipmap.login_bg)
            .transform(BlurTransformation(40), CenterCrop())
            .into(loginFlowBackground)
        tabLayout.addOnTabSelectedListener(tabSelectedListener)
        //Add focus change listener to every edittext in the form
        formInputFields.forEach {
            it.onFocusChangeListener = focusChangeListener
        }
        finishSignup.setOnClickListener {
            validateAndSignUp()
        }
        tilMobile.helperText = getString(R.string.til_helper_valid_phone)
    }

    /**
     * Validate form fields and create user then log in user
     */
    private fun validateAndSignUp() {
        val accountToCreate = getUserAuthFromFormFields()
        if (isFormValid) {
            accountToCreate?.signUp { successful, error ->
                //Create user operation was successful
                if (successful) {
                    //Use same credentials to login
                    accountToCreate.loginCredentials.logIn { success ->
                        //Login was successful
                        if (success) {
                            onUserSignedUp(accountToCreate)
                        }
                    }
                } else Toast.makeText(context, "Bir hata oluştu: $error", Toast.LENGTH_SHORT).show()
            }

        }
    }

    /**
     * Fire events when user signed up and logged in successfully
     */
    private fun onUserSignedUp(accountToCreate: Account) {
        Toast.makeText(
            context,
            String.format(
                getString(R.string.toast_welcome),
                accountToCreate.accountName
            ),
            Toast.LENGTH_LONG
        ).show()
        LocalData.with(context).write(ACTIVE_USER, accountToCreate.accountIdentityNumber)
        startActivity(Intent(this,LandingActivity::class.java))
    }

    /**
     * Create userauth object from form fields
     */
    private fun getUserAuthFromFormFields(): Account? {
        val name = inputName.text.toString()
        val accountName = name.split("").getOrNull(0).orEmpty()
        if (accountName.isEmpty())
            return null
        val accountSurname = accountName.replaceFirst(accountName, "").trimStart()
        if (accountSurname.isEmpty())
            return null
        val title = inputTitle.text.toString()
        val userId = inputUserid.text.toString()
        val email = inputEmail.text.toString()
        val mobile = inputMobile.text.toString()
        val password = inputPassword.text.toString()
        val type = when (selectedTab) {
            TAB_FIRM -> TYPE_FIRM
            else -> TYPE_PERSON
        }
        return Account(accountName, accountSurname, title, userId, email, mobile, password, type)
    }

    private val focusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
        //Validate after view loses focus
        if (hasFocus) return@OnFocusChangeListener
        //Get text selected from edittext
        val textToValidate = (v as TextInputEditText).text.toString()
        //Text Input Layout Parent of the edittext
        // *EditText.parent.parent somehow returns textinputlayout
        val tilParent = v.parent.parent as TextInputLayout
        tilParent.isErrorEnabled = false
        tilParent.error = ""
        if (textToValidate.isBlank()) {
            tilParent.isErrorEnabled = true
            tilParent.error = getString(R.string.til_error_blank)
        }
        when (v) {
            //Validate email
            inputEmail -> {
                if (textToValidate.isNotEmpty()) {
                    tilParent.isErrorEnabled = textToValidate.isValidEmail.not()
                    if (textToValidate.isValidEmail) tilEmail.error = "" else
                        tilParent.error = getString(R.string.til_error_invalid_email)
                }
            }
        }
    }

    private val tabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabReselected(p0: TabLayout.Tab?) {

        }

        override fun onTabUnselected(p0: TabLayout.Tab?) {

        }

        override fun onTabSelected(tabLayout: TabLayout.Tab?) {
            val tab = tabLayout?.position ?: TAB_FIRM
            selectedTabState(tab)
            selectedTab = tab
        }
    }

    private fun selectedTabState(selected: Int) {
        //Hide keyboard when selected tab changes
        dismissKeyboard(tilUserid.windowToken)
        tilUserid.error = ""
        tilUserid.isErrorEnabled = false
        when (selected) {
            TAB_FIRM -> {
                tilUserid.hint = getString(R.string.input_signup_tax)
            }
            TAB_PERSON -> {
                tilUserid.hint = getString(R.string.input_signup_tcid)
            }
        }
    }

    /**
     * Check if all fields are correctly filled
     */
    private val isFormValid: Boolean
        get() {
            //Check if all fields are not blank
            formInputFields.forEach {
                if (it.text.toString().isBlank()) {
                    val tilParent = it.parent.parent as TextInputLayout
                    tilParent.isErrorEnabled = true
                    tilParent.error = getString(R.string.til_error_blank)
                }
            }
            if (inputPassword.text.toString().isNotEmpty()) {
                if (inputPassword.text.toString() != inputPasswordRepeat.text.toString()) {
                    tilPasswordRepeat.isErrorEnabled = true
                    tilPasswordRepeat.error = getString(R.string.til_error_password_not_matched)
                }
            }
            //Check If there is an invalid Edittext in the form
            formInputFields.forEach {
                if ((it.parent.parent as TextInputLayout).isErrorEnabled) {
                    val til = it.parent.parent as TextInputLayout
                    //Focus on TIL that has invalid entry
                    nestedScrollView.smoothScrollTo(0, til.top)
                    return false
                }
            }
            return true
        }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            super.onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}
