package dev.into.etbstest.ui.fragment.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import dev.into.etbstest.R
import dev.into.etbstest.adapter.NavActionAdapter
import dev.into.etbstest.model.*
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() , NavAction.SelectedListener{


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.adapter = NavActionAdapter(NavAction.list,this)
    }

    override fun onSelected(action: Int) {

    }
}