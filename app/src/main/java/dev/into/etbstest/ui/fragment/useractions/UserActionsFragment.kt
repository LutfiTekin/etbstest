package dev.into.etbstest.ui.fragment.useractions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import dev.into.etbstest.R

class UserActionsFragment : Fragment() {

    private lateinit var userActionsViewModel: UserActionsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        userActionsViewModel =
            ViewModelProviders.of(this).get(UserActionsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_user_actions, container, false)
        val textView: TextView = root.findViewById(R.id.text_slideshow)
        userActionsViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}