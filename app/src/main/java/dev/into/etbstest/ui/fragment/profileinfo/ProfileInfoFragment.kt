package dev.into.etbstest.ui.fragment.profileinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import dev.into.etbstest.R

class ProfileInfoFragment : Fragment() {

    private lateinit var profileInfoViewModel: ProfileInfoViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        profileInfoViewModel =
            ViewModelProviders.of(this).get(ProfileInfoViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_user_info, container, false)
        val textView: TextView = root.findViewById(R.id.text_gallery)
        profileInfoViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }
}