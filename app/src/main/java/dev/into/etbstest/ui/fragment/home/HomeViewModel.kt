package dev.into.etbstest.ui.fragment.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.into.etbstest.R
import dev.into.etbstest.model.*

class HomeViewModel : ViewModel() {

    private val _list = MutableLiveData<List<NavAction>>().apply {
        value = listOf()
    }
    val text: LiveData<List<NavAction>> = _list
}