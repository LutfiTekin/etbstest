package dev.into.etbstest.ui.activity

import android.content.Intent
import android.os.Bundle
import dev.into.etbstest.R

class LandingActivity : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_landing)
        if (isLoggedIn){
            //TODO logged in
            startActivity(Intent(this, MainActivity::class.java))
        }else{
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

}
