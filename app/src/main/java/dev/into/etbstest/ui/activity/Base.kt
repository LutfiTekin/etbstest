package dev.into.etbstest.ui.activity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dev.into.etbstest.util.ACTIVE_USER
import dev.into.etbstest.util.LocalData
import dev.into.etbstest.util.logWith

open class Base : AppCompatActivity() {

    lateinit var context: Context
    var isLoggedIn : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = this
        isLoggedIn = LocalData.with(context).exists(ACTIVE_USER)
        "Cur Activity is: ${this.localClassName}" logWith "ETBS Base.kt"
    }




}
