package dev.into.etbstest.ui.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import dev.into.etbstest.R
import dev.into.etbstest.model.Account
import dev.into.etbstest.model.TaxIdRespone
import dev.into.etbstest.model.TokenRequest
import dev.into.etbstest.model.TokenResponse
import dev.into.etbstest.util.*
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.loginFlowBackground
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class LoginActivity : Base() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupUI()
    }

    /**
     * Setup UI elements before activity loads the ui
     */
    private fun setupUI() {
        //Load background image via glide with blur effect
        Glide.with(context)
            .load(R.mipmap.login_bg)
            .transform(BlurTransformation(40), CenterCrop())
            .into(loginFlowBackground)
        recoverPassword.text = getString(R.string.recover_password).fromHtmlCompat()
        newUser.setOnClickListener { startActivity(Intent(this,
            SignupActivity::class.java)) }
        login.setOnClickListener { getNameFromId() }
        recoverPassword.setOnClickListener { startActivity(Intent(this, RecoverPasswordActivity::class.java)) }
        getToken()
    }

    private fun getToken(){

        "get token called " logWith "Login activity"
        val retrofit = Retrofit.Builder()
            .baseUrl(ACCOUNT_BASE_URL)
            .client(defaultOkHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(AccountAuthService::class.java)

        //val serviceCall = service.getToken(pairs)
        val serviceCall = service.getToken2("password",
            "usr","pass")

        "url ${serviceCall.request().body()}" logWith "Login Activity"

        serviceCall.enqueue(object : Callback<TokenResponse>{
            override fun onResponse(call: Call<TokenResponse>, response: Response<TokenResponse>) {
                "Response :  $response ${response.body()}" logWith "Login Activity"
                response.body()?.let {
                    it.access_token logWith "Login Activity"
                    accessToken = it.access_token
                }

            }

            override fun onFailure(call: Call<TokenResponse>, t: Throwable) {
                "Fail : ${t.message}" logWith "Login Activity"
            }
        })
    }

    private fun userLogin(){
        val loginIdNumber = loginId.text.toString()
        val password = loginPassword.text.toString()
        val loginCredentials = Account.Login(loginIdNumber,password)
        loginCredentials.logIn {successful ->
            if (successful){
                startActivity(Intent(this,LandingActivity::class.java))
            }else Toast.makeText(context,R.string.try_again,Toast.LENGTH_SHORT).show()
        }
    }

    private fun getNameFromId(){
        val loginIdNumber = loginId.text.toString()
        val retrofit = Retrofit.Builder()
            .baseUrl(ACCOUNT_BASE_URL)
            .client(defaultOkHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(AccountAuthService::class.java)

        val serviceCall = service.getNameFromId(loginIdNumber, "Bearer $accessToken")

        serviceCall.enqueue(object : Callback<JsonObject>{
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    "response: $response ${response.body()}" logWith "LoginActivity"
                    loginId.setText(response.body()!!["Data"].toString())
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                "Fail : ${t.message}" logWith "Login Activity"
            }
        })
    }

companion object{
    var accessToken = ""
}


}


/**
        Animate if login successful
        Handler(Looper.getMainLooper()).postDelayed({
        Glide.with(context)
        .load(R.mipmap.login_bg)
        .placeholder(loginBackground.drawable)
        .transition(DrawableTransitionOptions.withCrossFade(300))
        .transform(BlurTransformation(1),CenterCrop())
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .into(loginBackground)

        },2300)

 */