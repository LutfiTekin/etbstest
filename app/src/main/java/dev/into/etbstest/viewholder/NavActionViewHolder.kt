package dev.into.etbstest.viewholder

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dev.into.etbstest.R
import dev.into.etbstest.model.NavAction

class NavActionViewHolder internal constructor(inflater: LayoutInflater,
                                               parent: ViewGroup,
                                               @LayoutRes res: Int,
                                               selectedListener: NavAction.SelectedListener?)
    : RecyclerView.ViewHolder(inflater.inflate(res, parent, false)){

    private lateinit var action: NavAction
    private var context: Context = parent.context
    private val icon = itemView.findViewById<ImageView>(R.id.icon)
    private val title = itemView.findViewById<TextView>(R.id.title)
    private val card = itemView.findViewById<CardView>(R.id.card)

    init {
        card.setOnClickListener {
            selectedListener?.onSelected(action.id)
        }
    }

    fun bind(navAction: NavAction){
        action = navAction
        Glide.with(context).load(action.icon).into(icon)
        title.text = context.getString(action.title)
    }
}