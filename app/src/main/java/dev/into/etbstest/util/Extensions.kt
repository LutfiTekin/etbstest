package dev.into.etbstest.util

import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.squareup.okhttp.Interceptor
import com.squareup.okhttp.Response
import dev.into.etbstest.BuildConfig
import dev.into.etbstest.model.Account
import dev.into.etbstest.ui.activity.LoginActivity

import okhttp3.OkHttpClient

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import java.util.concurrent.TimeUnit


fun String.fromHtmlCompat(): Spanned? {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(
        this,
        Html.FROM_HTML_MODE_LEGACY
    ) else Html.fromHtml(this)
}

fun Drawable.setColorFilterCompat(color: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
    } else {
        setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
    }
}

fun Context.dismissKeyboard(windowToken: IBinder) {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

val String.isValidEmail: Boolean
    get() {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

fun ViewGroup.beginDelayedTransition() {
    val duration = 300
    val autoTransition = AutoTransition()
    autoTransition.duration = duration.toLong()
    TransitionManager.beginDelayedTransition(this, autoTransition)
}

/**
 * Wait for test methods
 */
fun artificialDelay(maxDelay: Int = 4000, run: () -> Unit) {
    Handler(Looper.getMainLooper()).postDelayed({
        run()
    }, 500 + Random().nextInt(maxDelay).toLong())
}

fun testRecoverPassword(passwordReset: (Boolean) -> Unit) {
    artificialDelay {
        passwordReset(Random().nextBoolean())
    }
}

fun LocalData.testLogin(
    id: String,
    password: String,
    loggedIn: (Boolean) -> Unit
) {
    artificialDelay {
        //Local data key
        val userKey = "userauth${id}"
        //Check if user key exist in local database
        if (this.exists(userKey)) {
            write(ACTIVE_USER, userKey)
            loggedIn(read(userKey) == password)
        } else {
            loggedIn(false)
        }
    }
}




fun LocalData.testSignup(
    account: Account?,
    finishedWithoutError: (Boolean) -> Unit
) {
    if (account == null){
        finishedWithoutError(false)
        return
    }
    val userAuthKey = "userauth${account.accountIdentityNumber}"
    //User already exists
    if (exists(userAuthKey)) {
        finishedWithoutError(false)
        return
    }
    val userDataKey = "userdata${account.accountIdentityNumber}"
    writeObject(userDataKey, account)
    writeNow(userAuthKey, account.accountPassword)
    artificialDelay {
        finishedWithoutError(true)
    }
}

fun LocalData.logout(loggedOut: () -> Unit) {
    remove(ACTIVE_USER)
    loggedOut()
}

infix fun String.logWith(string: String){
    if (BuildConfig.DEBUG) Log.d("DEBUGLOG" +string,this)
}


fun String.getMD5(): String {
    try { // Create MD5 Hash
        val digest = MessageDigest.getInstance("MD5")
        digest.update(toByteArray())
        val messageDigest = digest.digest()
        // Create Hex String
        val hexString = StringBuilder()
        for (aMessageDigest in messageDigest) hexString.append(
            Integer.toHexString(
                0xFF and aMessageDigest.toInt()
            )
        )
        return hexString.toString()
    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
    }
    return ""
}

val defaultOkHttpClient: OkHttpClient
    get() {
        return OkHttpClient.Builder()
            .connectTimeout(3500, TimeUnit.MILLISECONDS)
            .writeTimeout(3500, TimeUnit.MILLISECONDS)
            .readTimeout(3500, TimeUnit.MILLISECONDS)
            .build()
    }