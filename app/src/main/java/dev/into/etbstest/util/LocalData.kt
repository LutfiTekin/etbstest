package dev.into.etbstest.util

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import java.lang.reflect.Type

const val SHARED_PREF = "shrdprf1"
const val ACTIVE_USER = "loggedinuser"

class LocalData(val context: Context) {


    val default: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(context)
    private val sharedPreferences: SharedPreferences
        get() = context.applicationContext.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor
        get() = sharedPreferences.edit()

    /**
     * Save a key value pair to shared pref
     * @param title
     * @param content
     */
    fun write(title: String, content: String?) = editor.putString(title, content).apply()

    fun writeNow(title: String, content: String?) = editor.putString(title, content).commit()

    /**
     * Save a key value pair to shared pref
     * @param title
     * @param content
     */
    fun write(title: String, content: Boolean) = editor.putBoolean(title, content).apply()

    /**
     * Remove a shared pref
     */
    fun remove(title: String) = editor.remove(title).apply()

    fun exists(title: String): Boolean = sharedPreferences.contains(title)


    fun read(res: Int, def: String): String {
        return default.getString(context.getString(res), def) ?: def
    }

    fun read(res: Int, def: Boolean): Boolean {
        return default.getBoolean(context.getString(res), def)
    }

    /**
     * Read value from shared prefs
     * @param title
     * @return
     */
    fun read(title: String): String? {
        return sharedPreferences.getString(title, null)
    }

    /**
     * Read value from shared prefs
     * @param title
     * @return
     */
    fun read(title: String, def: String): String = sharedPreferences.getString(title, def).orEmpty()

    fun writeObject(title: String, content: Any) =
        editor.putString(title, Gson().toJson(content)).apply()

    fun readObject(title: String, type: Type): Any? {
        val data = read(title)
        return Gson().fromJson(data, type)
    }


    /**
     * Read value from shared prefs
     * @param title
     * @return
     * @throws NullPointerException
     */
    fun read(title: String, def: Boolean): Boolean = sharedPreferences.getBoolean(title, def)

    fun read(title: String, def: Long): Long = sharedPreferences.getLong(title, def)

    fun write(title: String, content: Long) = editor.putLong(title, content).apply()

    companion object {
        fun with(context: Context): LocalData{
            return LocalData(context)
        }
    }

}