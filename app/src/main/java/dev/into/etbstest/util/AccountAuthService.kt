package dev.into.etbstest.util

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import dev.into.etbstest.model.*
import retrofit2.Call
import retrofit2.http.*

const val HOST = "http://testbkst.tarbil.gov.tr/Service//"
const val PORT = "8080"
const val ACCOUNT_BASE_URL = HOST

interface AccountAuthService {

    @FormUrlEncoded
    @POST("Token")
    fun getToken(tokenRequest: List<Pair<String,String>>): Call<JsonObject>


    @FormUrlEncoded
    @POST("Token")
    fun getToken2(@Field("grant_type") grant_type: String,
                  @Field("username") username: String,
                  @Field("password") password: String): Call<TokenResponse>


    @GET("api/main/GetRegisteredPerson")
    fun getNameFromId(@Query("IdTaxNo") id: String, @Header("Authorization") header: String): Call<JsonObject>

    @POST("create-account-users")
    fun createUser(@Body account: Account): Call<Result<String>>

    @FormUrlEncoded
    @POST("create-account-users")
    fun createUserWithForm(@FieldMap map: Map<String, String>): Call<Result<String>>

    @POST("get-account-user-password")
    fun loginUser(@Body email: String): Call<Result<String>>
}